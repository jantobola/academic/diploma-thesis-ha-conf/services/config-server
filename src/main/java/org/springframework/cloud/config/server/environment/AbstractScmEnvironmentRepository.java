/*
 * Copyright 2015-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.cloud.config.server.environment;

import com.jantobola.configserver.exception.ConfigProcessingException;
import com.jantobola.configserver.exception.ConfigValidationException;
import com.jantobola.configserver.validator.ValidationStatus;
import com.jantobola.configserver.validator.VaultValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.cloud.config.environment.Environment;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.cloud.config.server.support.AbstractScmAccessor;
import org.springframework.cloud.config.server.support.AbstractScmAccessorProperties;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.PropertyPlaceholderHelper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;

/**
 * @author Dave Syer, Jan Tobola
 */
public abstract class AbstractScmEnvironmentRepository extends AbstractScmAccessor
        implements EnvironmentRepository, SearchPathLocator, Ordered {

    private Optional<VaultEnvironmentRepository> vaultRepository;
    private Optional<VaultValidator> vaultValidator;

    private final String KEYWORD_INCLUDE = "@include";
    private final PropertyPlaceholderHelper privatePlaceholderHelper =
            new PropertyPlaceholderHelper("${{", "}}");

    private int order = Ordered.LOWEST_PRECEDENCE;
    private EnvironmentCleaner cleaner = new EnvironmentCleaner();

    public AbstractScmEnvironmentRepository(ConfigurableEnvironment environment) {
        super(environment);
    }

    public AbstractScmEnvironmentRepository(ConfigurableEnvironment environment, AbstractScmAccessorProperties properties) {
        super(environment, properties);
        this.order = properties.getOrder();
    }

    @Override
    public synchronized Environment findOne(String application, String profile, String label) {
        NativeEnvironmentRepository fsRepository = new NativeEnvironmentRepository(
                getEnvironment(), new NativeEnvironmentProperties());
        Locations locations = getLocations(application, profile, label);
        fsRepository.setSearchLocations(locations.getLocations());

        // Načtení veřejné části konfigurace
        Environment publicConf = fsRepository.findOne(application, profile, "");
        // Definice výsledného objektu
        Environment resultConf = new Environment(publicConf);

        // Zpracování klíčových slov @include
        for (PropertySource ps : publicConf.getPropertySources()) {
            List<Map.Entry<String, Object>> list = ps.getSource().entrySet().stream()
                    .filter(e -> e.getKey() instanceof String)
                    .map(e -> Map.entry((String) e.getKey(), (Object) e.getValue()))
                    .collect(toList());
            // rekurzivní algoritmus (viz kapitola 5.2.2.1)
            // předání první úrovně konfiguračních hodnot a cesty
            // adresáře na souborovém systému
            var all = handleIncludes(list, Paths.get(ps.getName()).getParent());
            var precedenceMap = new LinkedHashMap<String, Object>();
            // Zbavení duplicit, zachování precedence hodnot
            all.forEach(e -> precedenceMap.put(e.getKey(), e.getValue()));
            // Vložení konfigurace do finálního objektu
            resultConf.getPropertySources().add(
                    new PropertySource(ps.getName(), precedenceMap));
        }

        // Substituce privátních zástupných symbolů
        vaultRepository.ifPresent(repository -> {
            // Načtení privátní části konfigurace
            Environment vaultEnv = repository.findOne(application, profile, "");
            var secrets = new Properties();
            vaultEnv.getPropertySources().forEach(ps -> secrets.putAll(ps.getSource()));
            if (secrets.size() > 0) {
                // Mutace výsledného objektu - nahrazení privátních zástupných symbolů
                for (PropertySource ps : resultConf.getPropertySources()) {
                    Map<Object, Object> map = (Map<Object, Object>) ps.getSource();
                    for (var entry : map.entrySet()) {
                        if (entry.getKey() instanceof String
                                && entry.getValue() instanceof String) {
                            // Využití PropertyPlaceholderHelper z jádra Spring
                            // framework pro nahrazení symbolů
                            map.computeIfPresent(entry.getKey(), (k, v) ->
                                privatePlaceholderHelper.replacePlaceholders(
                                        (String) v, secrets));
                        }
                    }
                }
            }
        });

        // Validace výsledné konfigurace
        vaultValidator.ifPresent(validator -> {
            var validationResult = validator.validate(resultConf);
            if (validationResult.getStatus() == ValidationStatus.INVALID) {
                // Vyhození výjimky, předání chybových hlášek z validátoru
                throw new ConfigValidationException(validationResult.getErrors());
            }
        });

        resultConf.setVersion(locations.getVersion());
        resultConf.setLabel(label);
        return this.cleaner.clean(resultConf, getWorkingDirectory().toURI().toString(), getUri());
    }

    private List<Map.Entry<String, Object>> handleIncludes(
            List<Map.Entry<String, Object>> content, Path currentPath) {
        List<Map.Entry<String, Object>> afterIncludeContent = new ArrayList<>();
        // Pro každou hodnotu v souboru
        for (var it : content) {
            // Pokud klíč začíná klíčovým slovem @include
            if (it.getKey().startsWith(KEYWORD_INCLUDE)) {
                // korekce cesty a normalizace
                var includePath = Paths.get(currentPath.toString().startsWith("file:")
                        ? currentPath.toString().substring(5)
                        : currentPath.toString(), (String) it.getValue()
                ).normalize();
                // Pokud soubor existuje
                if (includePath.toFile().exists()) {
                    List<Map.Entry<String, Object>> includeContent;
                    // Pokud vkládaný soubor je formátu properties
                    if (includePath.toString().endsWith(".properties")) {
                        includeContent = handleFormat(includePath,
                                () -> loadProperties(includePath));
                    // Pokud vkládaný soubor je formátu YAML
                    } else if (includePath.toString().endsWith(".yml")
                            || includePath.toString().endsWith(".yaml")) {
                        includeContent = handleFormat(includePath,
                                () -> loadYaml(includePath));
                    } else {
                        // Nepodporovaný formát vkládaného souboru
                        throw new ConfigProcessingException("Cannot include '" +
                                includePath.getFileName().toString() +
                                "'. Unsupported file format.");
                    }
                    // Přidání výsledku do listu
                    afterIncludeContent.addAll(includeContent);
                } else {
                    // Pokud vkládaný soubor neexistuje, vyhození výjimky
                    throw new ConfigProcessingException("Cannot include '" +
                            includePath.getFileName().toString() +
                            "'. File does not exist.");
                }
            } else {
                // Pokud klíč nezačíná na @include, přidání hodnoty do listu
                afterIncludeContent.add(it);
            }
        }
        return afterIncludeContent;
    }

    private List<Map.Entry<String,Object>> handleFormat(
            Path includePath, Supplier<Properties> loadProps) {
        try {
            // Pokus o načtení properties
            Properties props = loadProps.get();
            if (CollectionUtils.isEmpty(props)) {
                return List.of();
            }
            // Konverze do listu
            var propEntries = props.entrySet().stream()
                    .map(e -> Map.entry((String) e.getKey(), e.getValue()))
                    .collect(toList());
            // Zanoření na nižší úroveň
            return handleIncludes(propEntries, includePath.getParent());
        } catch (Exception e) {
            // Soubor nebylo možné načíst
            throw new ConfigProcessingException("Cannot include '" + includePath
                    .getFileName().toString() + "' file. " + e.getMessage(), e);
        }
    }

    private Properties loadProperties(Path includePath) {
        return unchecked(() -> PropertiesLoaderUtils.loadProperties(
                new ByteArrayResource(Files.readAllBytes(includePath))));
    }

    private Properties loadYaml(Path includePath) {
        return unchecked(() -> {
            YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
            yaml.setResources(new ByteArrayResource(Files.readAllBytes(includePath)));
            return yaml.getObject();
        });
    }

    private static <T> T unchecked(ThrowingSupplier<T> throwingFunction) {
        try {
            return throwingFunction.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Autowired
    public void setVaultRepository(Optional<VaultEnvironmentRepository> vaultRepository) {
        this.vaultRepository = vaultRepository;
    }

    @Autowired
    public void setVaultValidator(Optional<VaultValidator> vaultValidator) {
        this.vaultValidator = vaultValidator;
    }

    /**
     * Helper interface
     */
    private interface ThrowingSupplier<T> {
        T get() throws Exception;
    }
}
