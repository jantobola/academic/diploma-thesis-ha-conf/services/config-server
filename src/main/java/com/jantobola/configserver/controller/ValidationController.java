package com.jantobola.configserver.controller;

import com.jantobola.configserver.exception.ConfigProcessingException;
import com.jantobola.configserver.exception.ConfigValidationException;
import com.jantobola.configserver.validator.ValidationResult;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.config.server.environment.EnvironmentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.jantobola.configserver.validator.ValidationStatus.INVALID;
import static com.jantobola.configserver.validator.ValidationStatus.VALID;

@RestController
@RequestMapping("/validation/{application}")
@RequiredArgsConstructor
public class ValidationController {

    private final EnvironmentRepository repository;

    @GetMapping
    public ResponseEntity<ValidationResult> validateConfiguration(
            @PathVariable String application) {
        return validate(application, "default", null);
    }

    @GetMapping("/{profiles}")
    public ResponseEntity<ValidationResult> validateConfiguration(
            @PathVariable String application, @PathVariable String profiles) {
        return validate(application, profiles, null);
    }

    @GetMapping("/{profiles}/{label}")
    public ResponseEntity<ValidationResult> validateConfiguration(
            @PathVariable String application, @PathVariable String profiles,
            @PathVariable String label) {
        return validate(application, profiles, label);
    }

    private ResponseEntity<ValidationResult> validate(String application,
                                                      String profiles, String label) {
        var builder = ValidationResult.builder().status(VALID);
        try {
            repository.findOne(application, profiles, label);
        } catch (ConfigProcessingException e) {
            builder.status(INVALID);
            builder.error(e.getMessage());
        } catch (ConfigValidationException e) {
            builder.status(INVALID);
            builder.errors(e.getErrors());
        }
        return ResponseEntity.ok(builder.build());
    }
}
