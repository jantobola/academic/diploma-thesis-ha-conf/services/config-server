package com.jantobola.configserver.validator;

import org.springframework.cloud.config.environment.Environment;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;

@Profile("vault")
@Component
public class VaultValidator {

    private final Pattern secretPlaceholderRegex = Pattern
            .compile(".*(\\$\\{\\{(.*)}}).*");

    public ValidationResult validate(Environment environment) {
        var missingSecrets = new ArrayList<String>();
        for (PropertySource ps : environment.getPropertySources()) {
            for (Map.Entry<?, ?> entry : ps.getSource().entrySet()) {
                if (entry.getValue() instanceof String) {
                    var matcher = secretPlaceholderRegex.matcher(
                            (String) entry.getValue());
                    if (matcher.matches()) {
                        var prop = matcher.group(2);
                        missingSecrets.add("Missing Vault secret value '" +
                                prop + "' for property '" + entry.getKey() + "'");
                    }
                }
            }
        }
        return missingSecrets.size() > 0
                ? ValidationResult.invalid(missingSecrets)
                : ValidationResult.valid();
    }
}
