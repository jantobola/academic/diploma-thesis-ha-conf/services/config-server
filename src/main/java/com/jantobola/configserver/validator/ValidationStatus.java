package com.jantobola.configserver.validator;

public enum ValidationStatus {

    VALID, INVALID

}
