package com.jantobola.configserver.validator;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValidationResult {

    private ValidationStatus status;
    @Singular
    private List<String> errors;

    public static ValidationResult valid() {
        return ValidationResult.builder()
                .status(ValidationStatus.VALID).build();
    }

    public static ValidationResult invalid(List<String> errors) {
        return ValidationResult.builder()
                .status(ValidationStatus.INVALID).errors(errors).build();
    }
}