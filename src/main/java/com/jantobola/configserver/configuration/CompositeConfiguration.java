package com.jantobola.configserver.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.config.server.environment.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class CompositeConfiguration {

    private List<EnvironmentRepository> environmentRepos = new ArrayList<>();

    @Bean
    @Primary
    @ConditionalOnBean(SearchPathLocator.class)
    public SearchPathCompositeEnvironmentRepository searchPathCompositeEnvironmentRepository() {
        return new SearchPathCompositeEnvironmentRepository(this.environmentRepos);
    }

    @Bean
    @Primary
    @ConditionalOnMissingBean(SearchPathLocator.class)
    public CompositeEnvironmentRepository compositeEnvironmentRepository() {
        return new CompositeEnvironmentRepository(this.environmentRepos);
    }

    @Autowired
    public void setEnvironmentRepos(List<EnvironmentRepository> repos) {
        this.environmentRepos = repos.stream()
                .filter(rep -> !(rep instanceof VaultEnvironmentRepository))
                .collect(Collectors.toList());
    }

}
