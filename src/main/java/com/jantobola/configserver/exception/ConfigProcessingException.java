package com.jantobola.configserver.exception;

public class ConfigProcessingException extends RuntimeException {

    public ConfigProcessingException(Throwable throwable) {
        super(throwable);
    }

    public ConfigProcessingException(String message) {
        super(message);
    }

    public ConfigProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
