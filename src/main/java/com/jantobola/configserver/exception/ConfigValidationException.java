package com.jantobola.configserver.exception;

import lombok.Data;

import java.util.List;

@Data
public class ConfigValidationException extends RuntimeException {

    private List<String> errors;

    public ConfigValidationException(List<String> errors) {
        super(String.join("; ", errors));
        this.errors = errors;
    }

    public ConfigValidationException(List<String> errors, Throwable cause) {
        super(String.join("; ", errors), cause);
        this.errors = errors;
    }
}
